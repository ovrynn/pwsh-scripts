param([string]$pattern="")

Write-Output 'Configuring logging level (default = Verbose)'
# TODO: Make logging dynamic and more sophisticated
$VerbosePreference = 'Continue'

[string]$cwd = Get-Location

Write-Output "Running mass-unpacker in $cwd..."

# TODO: Validate input pattern
if ($pattern -match '\.7z$')
{
    Write-Warning -Message 'Handling 7Zip archives. Installing 7Zip powershell module...'
    Install-Module -Name 7Zip4PowerShell -Scope CurrentUser
}

Write-Output "Looking for files matching $pattern"

$files = Get-ChildItem $cwd -Filter $pattern
$filesNo = $files.Length

Write-Debug -Message "$files"
Write-Output "Found a total of $filesNo files matching pattern $pattern"

$files | Foreach-Object {
    Write-Output "Processing file $_.FullName"
    Write-Verbose -Message "Unpacking $_.FullName"
    if ($pattern -match '\.7z$')
    {
	Expand-7Zip -ArchiveFileName $_.FullName -TargetPath $cwd\tmp
    } else {
        Expand-Archive -Path $_.FullName -DestinationPath $cwd\tmp
    }
    Write-Verbose -Message "Unpack successful, moving contents to $cwd"
    Push-Location tmp
    Move-Item -Path .\* -Destination $cwd
    Pop-Location
    Write-Verbose -Message 'Deleting temporary directory'
    Remove-Item -Path tmp
    Write-Verbose -Message 'Temporary directory deleted'
    Write-Verbose -Message 'Deleting original archive'
    Remove-Item -Path $_.FullName
    Write-Verbose -Message 'Original archive deleted'
}

# Reset logging level
Write-Output 'Resetting logging to defaults'
$VerbosePreference = 'SilentlyContinue'
Write-Output 'Script execution complete'

